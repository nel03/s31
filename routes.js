const http = require('http')

const port = 8000

const server = http.createServer(function(request, response){
    if (request.url == '/greeting'){
// You can use request.url to get the current destination of the user in the browser. you can also check if the current destination of the user matches with the endpoint and if it does, do the respective processes for each endpoint.
        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('Hello, batch 197!')
    } else if (request.url == '/homepage'){
        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('welcome to home page')
    } else { // if none of the endpoint match the current destination of the user, return a default value like 'Page not available'
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end('Page not available')
    }
})

server.listen(port)

console.log(`Server now accessible at localhost: ${port}`)